Useful code for the sector coupling project.
Included in this repository:
1, Sankey diagram generation from CSV files.
Quick change of the positions of nodes and values of links directly in the CSV files. 
Commands for generating links and nodes automatically, as well as for propagating colours from nodes to links will be uploaded later. 
